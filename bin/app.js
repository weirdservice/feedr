#!/usr/bin/env node
import program from 'commander'
import FeedParser from 'feedparser'
import request from 'request'
import crypto from 'crypto'

import pkg from '../package.json'

program
  .version(pkg.version)
  .usage('[options] <url>')
  .option('-H, --hash-only', 'return hash only')
  .parse(process.argv)

if (!program.args.length) {
  program.outputHelp()
  process.exit()
}

const url = program.args[0]
const req = request(url)
const feedparser = new FeedParser({feedurl: url})

const md5sum = (d) => {
  return crypto.createHash('md5').update(d).digest('hex')
}

const error = (err) => {
  console.log(JSON.stringify({status: "failed", message: err.message}))
}

req.on('error', error)

req.on('response', (res) => {
  if (res.statusCode != 200) {
    return req.emit('error', new Error(`Bad status ${res.statusCode}`))
  }
  req.pipe(feedparser)
})

feedparser.on('error', error)

feedparser.on('readable', () => {
  var stream = feedparser,
      meta = feedparser.meta,
      item

  while(item = stream.read()) {
    var result = {
      feed: url,
      link: item.link,
      hash: md5sum(item.title + item.description)
    }
    if (!program.hashOnly) {
      result.title = item.title
      result.description = item.description
    }
    console.log(JSON.stringify(result))
  }
})
