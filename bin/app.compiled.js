#!/usr/bin/env node
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _feedparser = require('feedparser');

var _feedparser2 = _interopRequireDefault(_feedparser);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

_commander2['default'].version(_packageJson2['default'].version).usage('[options] <url>').option('-H, --hash-only', 'return hash only').parse(process.argv);

if (!_commander2['default'].args.length) {
  _commander2['default'].outputHelp();
  process.exit();
}

var url = _commander2['default'].args[0];
var req = (0, _request2['default'])(url);
var feedparser = new _feedparser2['default']({ feedurl: url });

var md5sum = function md5sum(d) {
  return _crypto2['default'].createHash('md5').update(d).digest('hex');
};

var error = function error(err) {
  console.log(JSON.stringify({ status: "failed", message: err.message }));
};

req.on('error', error);

req.on('response', function (res) {
  if (res.statusCode != 200) {
    return req.emit('error', new Error('Bad status ' + res.statusCode));
  }
  req.pipe(feedparser);
});

feedparser.on('error', error);

feedparser.on('readable', function () {
  var stream = feedparser,
      meta = feedparser.meta,
      item;

  while (item = stream.read()) {
    var result = {
      feed: url,
      link: item.link,
      hash: md5sum(item.title + item.description)
    };
    if (!_commander2['default'].hashOnly) {
      result.title = item.title;
      result.description = item.description;
    }
    console.log(JSON.stringify(result));
  }
});
